import {useEffect, useState} from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'

function App() {
    const [count, setCount] = useState(0)
    const [serverText, setServerText] = useState<Record<string, any>>({});

    useEffect(() => {
        (async () => {
            const data = await fetch(import.meta.env.VITE_API_PATH)
            data.json().then(res => {
                setServerText(res)
            })
        })()
    }, [])

    return (
        <>
            <div>
                <a href="https://vitejs.dev" target="_blank">
                    <img src={viteLogo} className="logo" alt="Vite logo"/>
                </a>
                <a href="https://react.dev" target="_blank">
                    <img src={reactLogo} className="logo react" alt="React logo"/>
                </a>
            </div>
            <h1>Vite + React</h1>
            <div className="card">
                <button onClick={() => setCount((count) => count + 1)}>
                    count is {count}
                </button>
                <p>
                    Edit <code>src/App.tsx</code> and save to test HMR
                </p>
            </div>
            <>That is server text:</>
            <div>
                {serverText && Object.keys(serverText) && Object.keys(serverText).map(key =>
                    <div>{`${key}: ${serverText[key]}`}</div>)}
            </div>
        </>
    )
}

export default App
