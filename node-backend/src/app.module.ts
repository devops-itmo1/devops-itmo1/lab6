import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {join} from 'path';
import {getConnectionOptions} from 'typeorm';

@Module({
    imports: [
        TypeOrmModule.forRootAsync({
            useFactory: async () => {
                return Object.assign(await getConnectionOptions(), {
                    ssl: false,
                    entities: [join(__dirname, '**', '*.entity.{ts,js}')],
                })
            }
        }),
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}
