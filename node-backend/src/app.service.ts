import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello() {
    return {
      that: 'is',
      json: 'from',
      server: 1,
    };
  }
}
